# ISE starter
docker compose to run all services

# WARNING - WIP
Proof of concept/technodemo

Current status: working minimal demo set, but still absent some important parts

## todo
1. api sidecar with db, cbs file storage (like in gomicro)
2. persistent message broker (NATS)
3. other worker services in docker for different tools and scripts
4. validation
5. additional ui views (graph, table, dashboards)
6. tests
7. security
8. many more... :)

## build

run docker_build.sh to build docker images in ise_work and ise_ui

## run

docker-compose up -d

## use 

open localhost:8080, add new ip address in local network and run nmap scan with option vulners
